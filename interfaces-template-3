# Virtual cable with uplinks to example host at 10.0.0.1:2 and [fe::1]:2,
# and downlink ranges 10.1.0.0/16 (any port) and [fe::1:0:0/96]:3
#
# + creates a local tap named "example" with tunneling MTU (1450)
# + uses UDP port 3 for this cable end
# + reniced service daemon
# + single-v logging to /var/log/rrqnet-example.log
# + cable PSK /etc/rrqnet/keys/example[1-4].key (not included)
# + 10 packet buffers and a single dispatch thread
# + using static IPv4 assignment
#
# This template includes an extension block to manage a dnsmasq instance
# for serving dhcp (only). Usage:
# ifup exampe=dnsmasq
# ifdown exampe=dnsmasq

auto example
iface example inet manual
    mtu 1450
    address 192.168.222.2/24
    rrqnet_port 3
    rrqnet_nice -3
    rrqnet_log -v /var/log/rrqnet-example.log
    rrqnet_remote 10.0.0.1:2=/etc/rrqnet/keys/example1.key
    rrqnet_remote [fe::1]:2=/etc/rrqnet/keys/example2.key
    rrqnet_remote 10.1.0.0/16=/etc/rrqnet/keys/example3.key
    rrqnet_remote [fe::1:0:0/96]:3=/etc/rrqnet/keys/example4.key
    rrqnet_options -B 10 -T 1

iface dnsmasq inherits example
    up /usr/sbin/dnsmasq -i example -a 192.168.222.2 -I lo -I wlan0 -I eth0 \
       -p 0 -K -D -N -b --dhcp-sequential-ip \
       -F 192.168.222.100,192.168.222.200,255.255.255.0,192.168.222.255 \
       --dhcp-hostsfile=/etc/network/example.dhcphosts
    down pkill -f 'dnsmasq -i example'
