SBINDIR = $(DESTDIR)/usr/sbin
ETCDIR = $(DESTDIR)/etc/rrqnet
MAN1DIR = $(DESTDIR)/usr/share/man/man1
MAN8DIR = $(DESTDIR)/usr/share/man/man8
LINTIAN = $(DESTDIR)/usr/share/lintian/overrides

SBINCFILES = rrqnet
SBINFILES = rrqnet-cron $(SBINCFILES)
ETCFILES = set-source-route.sh ifupdown.sh
MAN1FILES = 
MAN8FILES = rrqnet.8 rrqnet-cron.8 rrqnet-ifupdown.sh.8
HTMLDOC = $(MAN8FILES:%=%.html)

.PHONY: ifupdown.sh

all: $(SBINFILES) $(ETCFILES) $(MAN1FILES) $(MAN8FILES) $(HTMLDOC)

squeezetest: squeeze.c squeezetest.c

$(HTMLDOC): %.html: %.adoc
	asciidoc -bhtml $^

$(MAN8FILES): %: %.adoc
	a2x -d manpage -f manpage $^

rrqnet: LDFLAGS += -lpthread
rrqnet: rrqnet.c htable.h htable.c sockaddr.h queue.h queue.c

rrqnet.E: rrqnet.c htable.c
	$(CC) -W -Wall $^ > $@

COMPILEOPTS = -g -W -Wall
#COMPILEOPTS = -pg -no-pie  -g -DGPROF

STATIC = -static
$(filter-out %.sh,$(SBINCFILES)): %: %.c 
	$(CC) $(COMPILEOPTS) $(STATIC) -o $@ $^ $(LDFLAGS)

.PHONY: clean
clean:
	rm -f $(filter-out %.sh,$(SBINCFILES))

# Installation targets

INSTALLTARGETS = $(addprefix $(SBINDIR)/,$(SBINFILES))
INSTALLTARGETS += $(addprefix $(ETCDIR)/,$(ETCFILES))
INSTALLTARGETS += $(addprefix $(MAN1DIR)/,$(MAN1FILES))
INSTALLTARGETS += $(addprefix $(MAN8DIR)/,$(MAN8FILES))
INSTALLTARGETS += $(LINTIAN)/rrqnet

#INSTALL = install -b -S orig
INSTALL = install

$(LINTIAN)/rrqnet: debian/lintian-overrides
	mkdir -p ${@:/rrqnet=}
	cp $^ $@

$(addprefix $(ETCDIR)/,conf.d keys):
	mkdir -p $@

$(ETCDIR)/ifupdown.sh: rrqnet-ifupdown.sh
	$(INSTALL) -D -T $< $@

$(SBINDIR)/% $(ETCDIR)/% $(MAN1DIR)/% $(MAN8DIR)/%: %
	$(INSTALL) -D -T $< $@

install: $(INSTALLTARGETS)

BUILDPACKAGE = -us -uc -G
deb:
	PREFIX= INCLUDE_PREFIX=/usr dpkg-buildpackage $(BUILDPACKAGE)
