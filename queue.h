#ifndef queue_H
#define queue_H

#include "sockaddr.h"
#include <semaphore.h>
#include <pthread.h>

typedef struct _QueueItem {
    struct _QueueItem *next;
    char data[];
} QueueItem;

typedef struct _Queue {
    QueueItem *head;
    QueueItem *last;
    sem_t count;
    pthread_mutex_t mutex;
} Queue;

extern void Queue_addItem(Queue *list,QueueItem *item);
extern QueueItem *Queue_getItem(Queue *list);
extern void Queue_initialize(Queue *list,int n,size_t size);

#endif

