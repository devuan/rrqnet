#ifndef sockaddr_H
#define sockaddr_H

#include <arpa/inet.h>
#include <sys/socket.h>

// IP Address union, with flag field
struct SockAddr {
    union {
	struct sockaddr in;
	struct sockaddr_in in4;
	struct sockaddr_in6 in6;
    };
};

#endif
